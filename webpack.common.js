const HtmlWebpackPlugin				= require('html-webpack-plugin');
const path										= require('path');
const pkg											= require('./package.json');
const { CleanWebpackPlugin }	= require('clean-webpack-plugin');

const resolve = dir => path.join(__dirname, dir);

module.exports = {
	entry: {
		'sortable': path.join(__dirname, 'src/index.js')
	},
	plugins: [
		new CleanWebpackPlugin(),
		new HtmlWebpackPlugin({
			title: pkg.name,
			template: path.join(__dirname, 'src/index.html')
		})
	],
	resolve: {
		extensions: [
			'.js',
			'.styl'
		],
		alias: {
			'~': resolve('src')
		}
	},
	module: {
		rules: [
			{
				enforce: 'pre',
				test: /\.js$/,
				loader: 'eslint-loader',
				exclude: /node_modules/
			},
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env']
					}
				}
			},
			{
				test: /\.svg$/,
				loader: 'svg-inline-loader'
			}
		]
	},
	output: {
		filename: '[name].js',
		path: path.resolve(__dirname, 'dist'),
		libraryTarget: 'var',
		library: 'sortable'
	}
};
