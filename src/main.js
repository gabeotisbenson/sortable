// Required modules
import downArrow from 'feather-icons/dist/icons/arrow-down.svg';
import moment from 'moment';
import upArrow from 'feather-icons/dist/icons/arrow-up.svg';

// Valid table class names
const classnames = {
	plain: 'sortable',
	arrows: 'sortable-with-arrows',
	sorted: {
		plain: 'sortable-sorted',
		asc: 'sortable-sorted-asc',
		desc: 'sortable-sorted-desc'
	}
};

// Function that takes in two values and returns a sort value
const getSortValue = (a, b) => {
	if (a > b) return 1;
	if (a === b) return 0;

	return -1;
};

// Function that takes in a heading and returns the direction in which that
// heading's column should be sorted.
const getHeadingDirection = heading => {
	// Selector for the direction attribute
	const dirAttr = 'data-sortable-direction';

	// Default the direction to ascending
	let direction = 'asc';

	// Remove any custom classes
	[classnames.sorted.plain, classnames.sorted.asc, classnames.sorted.desc]
		.forEach(classname => {
			heading.classList.remove(classname);
		});

	// Does the heading already have a direction attribute?  If yes, swap the
	// current direction.  If not, set the direction to ascending.
	if (heading.hasAttribute(dirAttr)) heading.setAttribute(dirAttr, heading.getAttribute(dirAttr) === 'asc' ? 'desc' : 'asc');
	else heading.setAttribute(dirAttr, direction);
	direction = heading.getAttribute(dirAttr);
	heading.classList.add(classnames.sorted.plain);
	heading.classList.add(classnames.sorted[direction]);

	return direction;
};

// Our actual sorting function
const sortTable = (table, heading, index) => {
	// Remove any currently-applied custom classes from headings
	const headings = [...table.querySelectorAll('th')];
	headings.forEach(head => {
		[classnames.sorted.plain, classnames.sorted.asc, classnames.sorted.desc]
			.forEach(classname => head.classList.remove(classname));
	});

	// Extract some constants from the table
	const tableBody = table.querySelector('tbody');
	const rows = tableBody.querySelectorAll('tr');
	const direction = getHeadingDirection(heading);

	// Arrow Handling
	// Get rid of all existing arrows
	if (table.classList.contains(classnames.arrows)) {
		table.querySelectorAll('thead tr th div.sortable-container div.feather-icon svg').forEach(arrow => {
			arrow.parentNode.removeChild(arrow);
		});
		heading.querySelector('div.sortable-container div.feather-icon').innerHTML = direction === 'asc' ? upArrow : downArrow;
	}

	// Create a sortable array from the rows passed in
	Array.prototype.slice.call(rows, 0)
		.sort((a, b) => {
			// Sort them using our custom sorting function
			// Get the text inside of the two cells
			const valA = a.querySelectorAll('td')[index].innerText.replace(/[^a-zA-Z0-9]/gu, '');
			const valB = b.querySelectorAll('td')[index].innerText.replace(/[^a-zA-Z0-9]/gu, '');

			if (!isNaN(valA) && !isNaN(valB)) {
				// If the two values are numbers, sort as numbers
				const aNum = Number(valA);
				const bNum = Number(valB);

				return direction === 'asc' ? getSortValue(aNum, bNum) : getSortValue(bNum, aNum);
			}
			if (moment(valA).isValid() && moment(valB).isValid()) {
				// Else if the two values are valid dates, sort as dates
				const aDate = moment(valA);
				const bDate = moment(valB);

				return direction === 'asc' ? getSortValue(aDate, bDate) : getSortValue(bDate, aDate);
			}

			// Else, just sort them as plain ole strings
			return direction === 'asc' ? getSortValue(valA, valB) : getSortValue(valB, valA);
		})
		.forEach(row => {
			// Then put the now sorted rows back in the table
			// eslint-disable-next-line prefer-destructuring
			const sortCell = [...row.querySelectorAll('td')][index];
			for (const key in classnames.sorted) {
				// eslint-disable-next-line prefer-destructuring
				const { [key]: sortClass } = classnames.sorted;
				row.querySelectorAll('td').forEach(cell => {
					cell.classList.remove(sortClass);
				});
			}
			sortCell.classList.add(classnames.sorted.plain);
			sortCell.classList.add(classnames.sorted[direction]);
			tableBody.appendChild(row);
		});
};

// Function that handles the initial setup for this whole thing
const attachListeners = () => {
	// Grab all tables that contain our designated selectors and loop through them
	const tables = [...document.querySelectorAll(`table.${classnames.plain}, table.${classnames.arrows}`)];
	for (let i = 0; i < tables.length; ++i) {
		// eslint-disable-next-line prefer-destructuring
		const table = tables[i];
		const headings = [...table.querySelectorAll('table thead tr th')];
		headings.forEach((heading, j) => {
			// If the table is using the arrows classname, we need to add some extra
			// html to the heading to display those arrows
			if (table.classList.contains(classnames.arrows)) heading.innerHTML = `
				<div class="sortable-container">
					<div>
						${heading.innerHTML}
					</div>
					<div class="feather-icon"></div>
				</div>
			`.trim();

			// When the heading gets clicked, run our sort function
			heading.addEventListener('click', () => {
				sortTable(table, heading, j);
			});
		});
	}
};

// Set our main function
const main = attachListeners;

// And export it
export default main;
