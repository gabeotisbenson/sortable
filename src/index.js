import main from '~/main';
import '~/style';

const ready = fn => {
	const documentReady = ['complete', 'loading'].indexOf(document.readyState) >= 0;
	if (documentReady) fn();
	else document.addEventListener('DOMContentLoaded', fn);
};

ready(main);
